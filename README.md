# <strong>Atividade Back-End

## Introdução

Autor: Pedro Henrique Figueiredo Von Zuben
<br>
Descrição: Repositório remoto feito para atividade back-end do treinamento técnico da EJCM (22.2).

<br>
<hr>
    
## Modelagem do Banco de Dados ##

Modelagem do Banco de Dados feita no <a href="https://github.com/chcandido/brModelo">brModelo<a/>.
        
<div align="center">

![Database Modeling](https://i.imgur.com/HhQgZO1.png)  

</div>
